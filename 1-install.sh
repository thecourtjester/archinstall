#!/bin/bash
clear
echo "    _             _       ___           _        _ _ "
echo "   / \   _ __ ___| |__   |_ _|_ __  ___| |_ __ _| | |"
echo "  / _ \ | '__/ __| '_ \   | || '_ \/ __| __/ _' | | |"
echo " / ___ \| | | (__| | | |  | || | | \__ \ || (_| | | |"
echo "/_/   \_\_|  \___|_| |_| |___|_| |_|___/\__\__,_|_|_|"
echo ""
echo "by Paul McCallum (2024)"
echo "forked from Stephan Raabe (2023)"
echo "-----------------------------------------------------"
echo ""
echo "Important: Please make sure that you have followed the "
echo "manual steps in the README to partition the harddisc!"
echo "Warning: Run this script at your own risk."
echo ""

# ------------------------------------------------------
# Enter partition names
# ------------------------------------------------------
lsblk
read -p "Enter the name of the EFI partition (eg. sda1): " sda1
read -p "Enter the name of the BOOT partition (eg. sda2): " sda2
read -p "Enter the name of the ROOT partition (eq. sda3): " sda3

# ------------------------------------------------------
# Sync time
# ------------------------------------------------------
timedatectl set-ntp true

# ------------------------------------------------------
# Format partitions
# ------------------------------------------------------
mkfs.vfat -n SYS /dev/$sda1;
mkfs.ext4 -F -L BOOT /dev/$sda2;
mkfs.btrfs -f -L ROOT /dev/$sda3

# ------------------------------------------------------
# Mount points for btrfs
# ------------------------------------------------------
mount /dev/$sda3 /mnt
btrfs su cr /mnt/@
btrfs su cr /mnt/@cache
btrfs su cr /mnt/@home
btrfs su cr /mnt/@images
btrfs su cr /mnt/@snapshots
btrfs su cr /mnt/@log
umount /mnt

mount -o compress=zstd:3,noatime,subvol=@ /dev/$sda3 /mnt
mkdir -p /mnt/{boot,home,.snapshots,var/{cache,log,lib/libvirt/images}}
mount /dev/$sda2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/$sda1 /mnt/boot/efi
mount -o compress=zstd:3,noatime,subvol=@cache /dev/$sda3 /mnt/var/cache
mount -o compress=zstd:3,noatime,subvol=@home /dev/$sda3 /mnt/home
mount -o compress=zstd:3,noatime,subvol=@log /dev/$sda3 /mnt/var/log
mount -o compress=zstd:3,noatime,subvol=@images /dev/$sda3 /mnt/var/lib/libvirt/images
mount -o compress=zstd:3,noatime,subvol=@snapshots /dev/$sda3 /mnt/.snapshots

# ------------------------------------------------------
# Install base packages
# ------------------------------------------------------
pacstrap -K /mnt base base-devel git linux linux-firmware vim nano openssh reflector rsync intel-ucode

# ------------------------------------------------------
# Generate fstab
# ------------------------------------------------------
genfstab -U /mnt >> /mnt/etc/fstab
cat /mnt/etc/fstab

# ------------------------------------------------------
# Install configuration scripts
# ------------------------------------------------------
mkdir /mnt/archinstall
cp 2-configuration.sh /mnt/archinstall/
cp 3-yay.sh /mnt/archinstall/
cp 4-zram.sh /mnt/archinstall/
cp 5-snapper.sh /mnt/archinstall/
cp 6-preload.sh /mnt/archinstall/
cp 7-kvm.sh /mnt/archinstall/

# ------------------------------------------------------
# Chroot to installed sytem
# ------------------------------------------------------
arch-chroot /mnt ./archinstall/2-configuration.sh
