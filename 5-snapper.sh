#!/bin/bash
# ____                                    
#/ ___| _ __   __ _ _ __  _ __   ___ _ __ 
#\___ \| '_ \ / _` | '_ \| '_ \ / _ \ '__|
# ___) | | | | (_| | |_) | |_) |  __/ |   
#|____/|_| |_|\__,_| .__/| .__/ \___|_|   
#                  |_|   |_|              
# by Paul McCallum (2023) 
# ----------------------------------------------------- 
# Snapper Install Script
# yay must be installed
# -----------------------------------------------------
# NAME: Snapper Installation
# DESC: Installation script for Snapper
# WARNING: Run this script at your own risk.

# -----------------------------------------------------
# Confirm Start
# -----------------------------------------------------
while true; do
    read -p "DO YOU WANT TO START THE INSTALLATION NOW? (Yy/Nn): " yn
    case $yn in
        [Yy]* )
            echo "Installation started."
        break;;
        [Nn]* ) 
            exit;
        break;;
        * ) echo "Please answer yes or no.";;
    esac
done

# -----------------------------------------------------
# Install snapper
# -----------------------------------------------------
yay --noconfirm -S snapper-support
sudo umount /.snapshots/
sudo rm -r /.snapshots/
sudo snapper -c root create-config /
sudo btrfs subvolume delete /.snapshots
sudo mkdir /.snapshots
sudo mount -av
sudo grub-mkconfig -o /boot/grub/grub.cfg
sudo systemctl enable --now grub-btrfsd

echo "DONE!"
echo "You can create snapshots and update the GRUB Bootloader with ./snapshot.sh"

